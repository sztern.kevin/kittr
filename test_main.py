from main import app

def test_index():
    c = app.test_client()
    ret = c.get("/")
    assert b"Hello kitty" in ret.data

def test_empty_kitties():
    c = app.test_client()
    ret = c.get("/kitty").get_json()
    assert ret['status'] == 'OK'
    assert ret['value'] == []

def test_add_kitty():
    c = app.test_client()
    ret = c.post("/kitty", data=dict(
        name='Leo'
        )).get_json()
    assert ret['status'] == 'OK'

    ret = c.get('/kitty').get_json()
    assert 'Leo' in ret['value']
