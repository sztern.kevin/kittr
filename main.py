import flask
from flask import Flask, jsonify

app = Flask(__name__)
all_kitties = []

@app.route("/")
def index():
    return "Hello kitty!"

@app.route("/kitty", methods=['GET', 'POST'])
def get_kitties():
    if flask.request.method == 'GET':
        return jsonify({'status': 'OK', 'value': all_kitties})
    else:
        all_kitties.append(flask.request.values.get('name'))
        return jsonify({'status': 'OK'})
